<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Song extends Model
{
    protected $fillable = ['gambar','lagu','judul','artis','genre','album'];
    public $timestamps = false;
}
