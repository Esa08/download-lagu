<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['users'] = User::all();
        return view('kelolaUser.daftar',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        $data['judul'] = 'Tambah';
        return view('kelolaUser.form',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'gambar' => 'mimes:jpg,jpeg,png',
            'nama' => 'required',
            'level' => 'required',
            'tgl' => 'required',
            'email' => 'required',
            'pwd' => 'required'
        ]);
        
        $gambar = Storage::putFile('gambarUser', $request->file('gambar'));
        User::create([
            'gambar' => $gambar,
            'nama' => $request->nama,
            'level' => $request->level,
            'tanggal_lahir' => $request->tgl,
            'email' => $request->email,
            'password' => $request->pwd
        ]);

        return redirect()->back()->with('status','Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $data['judul'] = "Detail";
        $data['user'] = User::find($user)->first();
        $data['gambar'] = asset('storage/'.$data['user']['gambar']);
        return view('kelolaUser.form',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $data['judul'] = "Edit";
        $data['user'] = User::find($user)->first();
        $data['gambar'] = asset('storage/'.$data['user']['gambar']);
        return view('kelolaUser.form',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'gambar' => 'mimes:jpg,jpeg,png',
            'nama' => 'required',
            'level' => 'required',
            'tgl' => 'required',
            'email' => 'required',
            'pwd' => 'required'
        ]);

        $qry = User::where('id', $user->id);
        
        if($request->file('gambar')){
            if($user->gambar){
                Storage::delete($user->gambar);
            }
            $gambar = Storage::putFile('gambarUser', $request->file('gambar'));
            $qry->update( ['gambar' => $gambar] );
        }

        $qry->update([
            'nama' => $request->nama,
            'level' => $request->level,
            'tanggal_lahir' => $request->tgl,
            'email' => $request->email,
            'password' => $request->pwd
        ]);
            
        return redirect()->back()->with('status','Data Berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        Storage::delete($user->gambar);
        User::destroy($user->id);

        return redirect()->back()->with('status','Data Berhasil dihapus');
    }
}
