<?php

namespace App\Http\Controllers;

use App\Song;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SongController extends Controller
{
    public function index()
    {
        $data['songs'] = Song::all();
        return view('kelolaLagu.daftar',$data);
    }

    public function create()
    {
        $data['judul'] = 'Tambah';
        return view('kelolaLagu.form',$data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'gambar' => 'mimes:jpg,png,jpeg',
            'lagu' => 'mimes:mpga',
            'judul' => 'required',
            'artis' => 'required',
            'genre' => 'required',
            'album' => 'required'
        ]);
        
        $gambar = Storage::putFile('gambarLagu', $request->file('gambar'));
        $lagu = Storage::putFile('lagu', $request->file('lagu'));

        Song::create([
            'gambar' => $gambar,
            'lagu' => $lagu,
            'judul' => $request->judul,
            'artis' => $request->artis,
            'genre' => $request->genre,
            'album' => $request->album,
        ]);

        return redirect()->back()->with('status','Data berhasil ditambahkan');
    }

    public function show(Song $song)
    {   
        $data['judul'] = "Detail";
        $data['songs'] = Song::find($song)->first();
        $data['gambar'] = asset('storage/'.$data['songs']['gambar']);
        return view('kelolaLagu.form',$data);
    }

    public function edit(Song $song)
    {
        
        $data['judul'] = 'Edit';
        $data['songs'] = Song::find($song)->first();
        $data['gambar'] = asset('storage/'.$data['songs']['gambar']);
        return view('kelolaLagu.form',$data);
    }

    public function update(Request $request, Song $song)
    {
        // memvalidasi
        $request->validate([
            'gambar' => 'mimes:jpg,png,jpeg',
            'lagu' => 'mimes:mp3',
            'judul' => 'required',
            'artis' => 'required',
            'genre' => 'required',
            'album' => 'required'
        ]);
        
        // UBAH DATA
        $qry = Song::where('id', $song->id);

        // jika ada gambar dalam request
        if($request->file('gambar')){
            Storage::delete($song->gambar);
            $gambar = $request->file('gambar')->store('gambarLagu');
            $qry->update( ['gambar' => $gambar] );
        }

        //jika ada lagu dalam request
        if($request->file('lagu')){
            Storage::delete($song->lagu);
            $lagu = $request->file('lagu')->store('lagu');
            $qry->update( ['lagu' => $lagu] );
        }

        $qry->update([
            'judul' => $request->judul,
            'artis' => $request->artis,
            'genre' => $request->genre,
            'album' => $request->album
        ]);
        // END UBAH DATA
        
        return redirect()->back()->with('status','Data Berhasil diubah');
    }

    public function destroy(Song $song)
    {

        Storage::delete($song->gambar);
        Song::destroy($song->id);

        return redirect()->back()->with('status','Data Berhasil dihapus');
    }
}
