<?php

namespace App\Http\Controllers;

use App\Song;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DownloadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['songs'] = Song::all(); 
        return view('download.daftar',$data);
    }
    public function info(Song $song)
    {
        $data['song'] = Song::find($song)->first(); 
        return view('download.info',$data);
    }
    public function download(Request $request)
    {
        return Storage::download($request->lagu);
    }
}
