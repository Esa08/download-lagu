@extends('template.dashboard')

@section('title','Laguku')
@section('pencarian')
    <form method='post' class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="search" placeholder="Lagu, artis, genre, album" aria-label="Search">
        <button class="btn btn-success my-2 my-sm-0" type="submit">Search</button>
    </form>
@endsection



@section('content')
    <div class='row'>
    @foreach($songs as $song)
        <div class='col-md-2'>
            <a href="{{url('')}}/umum/info/{{$song['id']}}">
                <div class="card">
                    <img src="{{asset('storage/'.$song['gambar'])}}" class="card-img-top" height='100px'>
                    <div class="card-body">
                        <p class="card-text">{{$song['judul']}}</p>
                    </div>
                </div>
            </a>
        </div>
    @endforeach
    </div>
@endsection