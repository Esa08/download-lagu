@extends('template.dashboard')

@section('title','Info Lagu')
@section('pencarian')
    <form method='post' class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="search" placeholder="Lagu, artis, genre, album" aria-label="Search">
        <button class="btn btn-success my-2 my-sm-0" type="submit">Search</button>
    </form>
@endsection



@section('content')
    <div class='row'>
        <div class='col-md-6'>
            <div class="media">
                <img src="{{asset('storage/'.$song['gambar'])}}" class="mr-3" width='100px'>
                <div class="media-body">
                    <table>
                        <tr>
                            <td>Judul</td>
                            <td>: {{$song['judul']}}</td>
                        </tr>
                        <tr>
                            <td>Artis</td>
                            <td>: {{$song['artis']}}</td>
                        </tr>
                        <tr>
                            <td>Genre</td>
                            <td>: {{$song['genre']}}</td>
                        </tr>
                        <tr>
                            <td>Album</td>
                            <td>: {{$song['']}}</td>
                        </tr>
                    </table>
                    <form action="{{url('')}}/umum/download" method="post">
                        @csrf
                        <input type='hidden' name='lagu' value='{{$song['lagu']}}'>
                        <button type='submit' class='btn btn-success'>Download</button>
                    </form>
                    <!-- <a href='{{url('')}}/umum/download/{{$song['lagu']}}' class='btn btn-success'>Download lagu</a> -->
                </div>
            </div>
        </div>
        <div class='col-md-6'>
            <audio controls>
                <source src="{{asset('storage/'.$song['lagu'])}}" type="audio/mpeg">
            </audio>
        </div>
    </div>
@endsection