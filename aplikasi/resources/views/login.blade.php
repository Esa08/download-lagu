@extends('template.dashboard')

@section('title','Login')



@section('content')
    <div class='row justify-content-center'>
        <div class='col-md-9 align-self-center'>
            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-6">
                        <img src="img/" class="card-img">
                    </div>
                    <div class="col-md-6">
                        <div class="card-body">
                            <form action="/login" method='post'>
                                <div class="form-group">
                                    <input type="email" name='email' class="form-control text-center" id="email" placeholder='Email'>
                                </div>
                                <div class="form-group">
                                    <input type="password" name='pwd' class="form-control text-center" id="pwd" placeholder='Password'>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">Masuk</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection