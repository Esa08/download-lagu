@extends('layouts.app')

@section('title','Daftar Lagu')

@section('content')
    <a href="{{url('')}}/kelolaLagu/tambah" class='btn btn-success'>Tambah</a> 
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="row">
        @foreach($songs as $song)
        <div class="col-sm-4">
            <div class="card">
                <div class="card-body">
                    <p>{{$song['judul']}} - {{$song['artis']}}</p>
                    <a href="{{url('')}}/kelolaLagu/detail/{{$song['id']}}" class="btn btn-info">Detail</a>
                    <a href="{{url('')}}/kelolaLagu/edit/{{$song['id']}}" class="btn btn-primary">Edit</a>
                    <a href="{{url('')}}/kelolaLagu/hapus/{{$song['id']}}" class="btn btn-danger">Hapus</a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
@endsection