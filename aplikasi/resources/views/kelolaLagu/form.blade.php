@extends('layouts.app')

@section('title',"$judul Lagu")

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header ">
                    <label clas="float-left">{{$judul}} Lagu</label>
                    <a href='/kelolaLagu' class='float-right btn btn-primary btn-sm'>Kembali</a>
                </div>

                <div class="card-body">
                    <form action="{{url('')}}/{{$judul}}Lagu/@if($judul!='Tambah'){{$songs['id']}}@endif" method='post' enctype='multipart/form-data' autocomplete='off'>
        @csrf
        @if($judul=='Detail')<fieldset disabled>@endif
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            @if ($judul!='Tambah')
                <img src='{{$gambar}}' class='' width='70px'>
            @endif
            @if($judul!='Detail')
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Gambar</label>
                <div class="col-sm-6">
                    <div class="custom-file">
                        <input type="file" name='gambar' class="custom-file-input @error('gambar') is-invalid @enderror" id="gambar">
                        <label class="custom-file-label" for="gambar">Pilih file gambar..</label>
                        @error('gambar') <div class="invalid-feedback">{{$message}}</div> @enderror
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Lagu</label>
                <div class="col-sm-6">
                    <div class="custom-file">
                        <input type="file" name='lagu' class="custom-file-input @error('lagu') is-invalid @enderror" id="lagu">
                        <label class="custom-file-label" for="lagu">Pilih file lagu..</label>
                        @error('lagu') <div class="invalid-feedback">{{$message}}</div> @enderror
                    </div>
                </div>
            </div>
            @endif
            <div class="form-group row">
                <label for="judul" class="col-sm-2 col-form-label">Judul</label>
                <div class="col-sm-6">
                    <input type="text" name='judul' class="form-control @error('judul') is-invalid @enderror" id="judul" @if($judul=='Tambah') value="{{ old('judul') }}" @endif @if($judul=='Detail' or $judul=='Edit')value="{{$songs['judul']}}"  @if($judul=='Detail') disabled @endif  @endif>
                    @error('judul') <div class="invalid-feedback">{{$message}}</div> @enderror
                </div>
            </div>
            <div class="form-group row">
                <label for="artis" class="col-sm-2 col-form-label">Artis</label>
                <div class="col-sm-6">
                    <input type="text" name='artis' class="form-control @error('artis') is-invalid @enderror" id="artis" @if($judul=='Tambah') value="{{ old('artis') }}" @endif @if($judul=='Detail' or $judul=='Edit') value="{{$songs['artis']}}"} @if($judul=='Detail') disabled @endif  @endif>
                    @error('artis') <div class="invalid-feedback">{{$message}}</div> @enderror
                </div>
            </div>
            <div class="form-group row">
                <label for="genre" class="col-sm-2 col-form-label">Genre</label>
                <div class="col-sm-4">
                    @if($judul=='tambah')
                    <select name="genre" class="custom-select @error('genre') is-invalid @enderror" id="genre">
                        <option>PILIH</option>
                        <option value='Pop'>Pop</option>
                        <option value='Reggae'>Reggae</option>
                        <option value='Dangdut'>Dangdut</option>
                        <option value='Jazz'>Jazz</option>
                        <option value='Rock'>Rock</option>
                        <option value='Hiphop'>Hiphop</option>
                    </select>
                    @else
                    <input type="text" name='genre' class="form-control @error('genre') is-invalid @enderror" id="genre" @if($judul!='Tambah')value="{{$songs['genre']}}"@endif @if($judul=='Detail') disabled @endif>
                    @endif
                    @error('genre') <div class="invalid-feedback">{{$message}}</div> @enderror
                </div>
            </div>
            <div class="form-group row">
                <label for="album" class="col-sm-2 col-form-label">Album</label>
                <div class="col-sm-4">
                    <input type="text" name='album' class="form-control @error('album') is-invalid @enderror" id="album" @if($judul!='Tambah') value="{{$songs['album']}}"  @endif>
                    @error('album') <div class="invalid-feedback">{{$message}}</div> @enderror
                </div>
            </div>
            @if($judul!='Detail')
            <div class="form-group row">
                <div class="col-sm-10 offset-2">
                    <button type='submit' class='btn btn-success'>Simpan</button>
                </div>
            </div>
            @endif
        @if($judul=='Detail') </fieldset> @endif
    </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>
    
@endsection