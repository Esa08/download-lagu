@extends('template.dashboard')

@section('title',"$judul User")

@section('content')
    <form action="{{url('')}}/{{$judul}}User/@if($judul!='Tambah'){{$user['id']}}@endif" method='post' enctype='multipart/form-data' autocomplete='off'>
        @csrf
        <a href='/kelolaUser' class='btn btn-primary'>Kembali</a>
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        @if ($judul!='Tambah')
            <img src='{{$gambar}}' class='' width='70px'>
        @endif
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Foto</label>
            <div class="col-sm-4">
                <input type="file" name='gambar' class="@error('judul') is-invalid @enderror" @if($judul=='Tambah') value="{{ old('gambar') }}" @endif @if($judul=='Detail') disabled @endif>
                @error('judul') <div class="invalid-feedback">{{$message}}</div> @enderror
            </div>
        </div>
        <div class="form-group row">
            <label for="nama" class="col-sm-2 col-form-label">Nama</label>
            <div class="col-sm-4">
                <input type="text" name='nama' class="form-control @error('judul') is-invalid @enderror" id="nama" @if($judul=='Tambah') value="{{ old('nama') }}" @endif @if($judul=='Detail' or $judul=='Edit')value="{{$user['nama']}}"  @if($judul=='Detail') disabled @endif  @endif>
                @error('nama') <div class="invalid-feedback">{{$message}}</div> @enderror
            </div>
        </div>
        <div class="form-group row">
            <label for="level" class="col-sm-2 col-form-label">Level</label>
            <div class="col-sm-4">
                <input type="text" name='level' readonly class="form-control @error('level') is-invalid @enderror" id="level" @if($judul=='Tambah') value="Operator" @endif @if($judul=='Detail' or $judul=='Edit') value="{{$user['level']}}"} @if($judul=='Detail') disabled @endif  @endif>
                @error('level') <div class="invalid-feedback">{{$message}}</div> @enderror
            </div>
        </div>
        <div class="form-group row">
            <label for="tgl" class="col-sm-2 col-form-label">Tanggal</label>
            <div class="col-sm-3">
                <input type="date" name='tgl' class="form-control @error('tgl') is-invalid @enderror" id="tgl" @if($judul=='Tambah') value="{{ old('tgl') }}" @endif @if($judul=='Detail' or $judul=='Edit') value="{{$user['tanggal_lahir']}}"} @if($judul=='Detail') disabled @endif  @endif>
                @error('tgl') <div class="invalid-feedback">{{$message}}</div> @enderror
            </div>
        </div>
        <div class="form-group row">
            <label for="email" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-3">
                <input type="text" name='email' class="form-control @error('email') is-invalid @enderror" id="email" @if($judul=='Tambah') value="{{ old('email') }}" @endif @if($judul=='Detail' or $judul=='Edit') value="{{$user['email']}}"} @if($judul=='Detail') disabled @endif  @endif>
                @error('email') <div class="invalid-feedback">{{$message}}</div> @enderror
            </div>
        </div>
        <div class="form-group row">
            <label for="pwd" class="col-sm-2 col-form-label">Password</label>
            <div class="col-sm-3">
                <input type="password" name='pwd' class="form-control @error('pwd') is-invalid @enderror" id="pwd" @if($judul=='Tambah') value="{{ old('pwd') }}" @endif @if($judul=='Detail' or $judul=='Edit') value="{{$user['password']}}"} @if($judul=='Detail') disabled @endif  @endif>
                @error('pwd') <div class="invalid-feedback">{{$message}}</div> @enderror
            </div>
        </div>
        @if($judul!='Detail')
            <div class="form-group row">
                <div class="col-sm-10 offset-2">
                    <button type='submit' class='btn btn-success'>Simpan</button>
                </div>
            </div>
        @endif
    </form>
@endsection