@extends('template.dashboard')

@section('title','Daftar User')

@section('content')
    <a href='{{url('')}}/kelolaUser/tambah' class='btn btn-success'>Tambah</a>
    <div class='row'>
    @foreach($users as $user)
        <div class='col-md-4'>
            <div class="card">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="{{asset('storage/'.$user['gambar'])}}" class="card-img">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title">{{$user['nama']}}</h5>
                            <p class="card-text">{{$user['email']}}</p>
                            <a href='{{url('')}}/kelolaUser/detail/{{$user['id']}}' class='btn btn-info'>Detail</a>
                            <a href='{{url('')}}/kelolaUser/edit/{{$user['id']}}' class='btn btn-primary'>Edit</a>
                            <a href='{{url('')}}/kelolaUser/hapus/{{$user['id']}}' class='btn btn-danger'>Hapus</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    </div>
@endsection