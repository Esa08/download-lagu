<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return view('auth.login');
});

Route::get('/kelolaUser', 'UserController@index');
Route::get('/kelolaUser/tambah', 'UserController@create');
Route::get('/kelolaUser/detail/{user}', 'UserController@show');
Route::get('/kelolaUser/edit/{user}', 'UserController@edit');
Route::get('/kelolaUser/hapus/{user}', 'UserController@destroy');
Route::post('/TambahUser', 'UserController@store');
Route::post('/EditUser/{user}', 'UserController@update');

Route::get('/kelolaLagu', 'SongController@index');
Route::get('/kelolaLagu/tambah', 'SongController@create');
Route::get('/kelolaLagu/detail/{song}', 'SongController@show');
Route::get('/kelolaLagu/edit/{song}', 'SongController@edit');
Route::get('/kelolaLagu/hapus/{song}', 'SongController@destroy');
Route::post('/TambahLagu', 'SongController@store');
Route::post('/EditLagu/{song}', 'SongController@update');

Route::get('/umum', 'DownloadController@index');
Route::get('/umum/info/{song}', 'DownloadController@info');
Route::post('/umum/download', 'DownloadController@download');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

